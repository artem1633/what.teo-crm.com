<?php 
namespace app\components;

use app\models\ClientWh; 
use app\models\ClientTg; 
use app\models\Message; 
use app\models\Recalls; 
use app\models\Mailing; 
  

use app\models\forms\ReportSearch;
use kartik\grid\GridView;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\VarDumper;
use yii\web\JsExpression;

/**
 * Class ComponentReport
 * @package app\components
 */
class ComponentReport extends Component
{
    const DATE_COLUMNS = [
        'client_wh_name',
        'client_wh_phone',
        'client_wh_create_at',
        'client_wh_tag',
        'client_tg_name',
        'client_tg_phone',
        'client_tg_create_at',
        'client_tg_step',
        'client_tg_other',
        'client_tg_wh',
        'message_client_wh_id',
        'message_text',
        'message_create_at',
        'recalls_client_wh_id',
        'recalls_type',
        'recalls_text',
        'recalls_create_at',
        'recalls_who',
        'mailing_name',
        'mailing_text',
        'mailing_status',
  
    ];

    /**
     * @var array
     */
    public $columns;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @param boolean $excel
     * @return array
     */
    public function getGridColumns($excel = false)
    {
        $btnClear = '<button class="btn btn-xs btn-default btn-block" onclick="$(this).parent().find(\'input\').val(\'\'); $(this).parent().find(\'input\').one().trigger(\'change\')">×</button>';

        for ($i = 0; $i < count($this->columns); $i++){
            $column = $this->columns[$i];
  
  
            if($column['attribute'] == 'message_client_wh_id'){
                $this->columns[$i]['filter'] = ArrayHelper::map(ClientWh::find()->all(), 'id', 'name');
                $this->columns[$i]['value'] = function($model){
                    $it = ClientWh::findOne($model['message_client_wh_id']);
                    if($it != null){
                        return $it->name;
                    }
                };
                $this->columns[$i]['filterType'] = GridView::FILTER_SELECT2;
                $this->columns[$i]['filterWidgetOptions'] = [
                    'options' => ['prompt' => '', 'multiple' => true],
                    'pluginOptions' => ['allowClear' => true],
                ];
            }

  
            if($column['attribute'] == 'recalls_client_wh_id'){
                $this->columns[$i]['filter'] = ArrayHelper::map(ClientWh::find()->all(), 'id', 'name');
                $this->columns[$i]['value'] = function($model){
                    $it = ClientWh::findOne($model['recalls_client_wh_id']);
                    if($it != null){
                        return $it->name;
                    }
                };
                $this->columns[$i]['filterType'] = GridView::FILTER_SELECT2;
                $this->columns[$i]['filterWidgetOptions'] = [
                    'options' => ['prompt' => '', 'multiple' => true],
                    'pluginOptions' => ['allowClear' => true],
                ];
            }

            if($column['attribute'] == 'recalls_who'){
                $this->columns[$i]['filter'] = ArrayHelper::map(ClientTg::find()->all(), 'id', 'name');
                $this->columns[$i]['value'] = function($model){
                    $it = ClientTg::findOne($model['recalls_who']);
                    if($it != null){
                        return $it->name;
                    }
                };
                $this->columns[$i]['filterType'] = GridView::FILTER_SELECT2;
                $this->columns[$i]['filterWidgetOptions'] = [
                    'options' => ['prompt' => '', 'multiple' => true],
                    'pluginOptions' => ['allowClear' => true],
                ];
            }

  
  
  
            if(isset($this->columns[$i]['filterType']) == false && isset($this->columns[$i]['filter']) == false){
                $value = ArrayHelper::getValue($_GET, "ReportSearch.".$this->columns[$i]['attribute']);
                $this->columns[$i]['filterOptions'] = ['style' => 'position: relative;'];
                $this->columns[$i]['filter'] = "<input type=\"text\" value=\"{$value}\" class=\"form-control\" name=\"ReportSearch[".$this->columns[$i]['attribute']."]\">".
                    '<span style="float: left; position: absolute; top: 5px; right: 0; font-size: 15px; cursor: pointer;" onclick="$(this).parent().find(\'input\').val(\'\'); $(this).parent().find(\'input\').one().trigger(\'change\');">x</span>';
            }
        }

        return $this->columns;
    }
}