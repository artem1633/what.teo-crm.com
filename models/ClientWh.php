<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
* This is the model class for table "client_wh".
*
    * @property string $name Имя
    * @property string $phone Телефон
    * @property  $create_at Создан
    * @property string $tag Хэш тэк
*/
class ClientWh extends \yii\db\ActiveRecord
{

        

    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'client_wh';
    }

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['name', 'phone', 'create_at', 'tag'], 'string'],
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'phone' => 'Телефон',
            'create_at' => 'Создан',
            'tag' => 'Хэш тэк',
        ];
    }

    

    /**
    * {@inheritdoc}
    */
    public function beforeSave($insert) {



    return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {

    }


    
    
    
    
            

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getMessages()
    {
        return $this->hasMany(Messages::className(), ['client_wh_id' => 'id']);
    }
            

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getRecallss()
    {
        return $this->hasMany(Recallss::className(), ['client_wh_id' => 'id']);
    }
                                                
}