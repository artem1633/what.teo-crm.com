<?php 
namespace app\models;

use Yii;

/**
 * This is the model class for table "role".
 *
 * @property int $id
 * @property string $name Название
 * @property integer $client_wh_create Пвотцап Создание
 * @property integer $client_wh_update Пвотцап Изменение
 * @property integer $client_wh_view Пвотцап Просмотр
 * @property integer $client_wh_view_all Пвотцап Просмотр всех
 * @property integer $client_wh_delete Пвотцап Удаление
 * @property integer $client_tg_create Птелеграмм Создание
 * @property integer $client_tg_update Птелеграмм Изменение
 * @property integer $client_tg_view Птелеграмм Просмотр
 * @property integer $client_tg_view_all Птелеграмм Просмотр всех
 * @property integer $client_tg_delete Птелеграмм Удаление
 * @property integer $message_create Сообщения Создание
 * @property integer $message_update Сообщения Изменение
 * @property integer $message_view Сообщения Просмотр
 * @property integer $message_view_all Сообщения Просмотр всех
 * @property integer $message_delete Сообщения Удаление
 * @property integer $recalls_create Отзовы Создание
 * @property integer $recalls_update Отзовы Изменение
 * @property integer $recalls_view Отзовы Просмотр
 * @property integer $recalls_view_all Отзовы Просмотр всех
 * @property integer $recalls_delete Отзовы Удаление
 * @property integer $mailing_create Рассылка Создание
 * @property integer $mailing_update Рассылка Изменение
 * @property integer $mailing_view Рассылка Просмотр
 * @property integer $mailing_view_all Рассылка Просмотр всех
 * @property integer $mailing_delete Рассылка Удаление
 * @property integer $books Справочники
 *
 * @property User[] $users
 */
class Role extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'role';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_wh_create', 'client_wh_update', 'client_wh_view', 'client_wh_view_all', 'client_wh_delete', 'client_tg_create', 'client_tg_update', 'client_tg_view', 'client_tg_view_all', 'client_tg_delete', 'message_create', 'message_update', 'message_view', 'message_view_all', 'message_delete', 'recalls_create', 'recalls_update', 'recalls_view', 'recalls_view_all', 'recalls_delete', 'mailing_create', 'mailing_update', 'mailing_view', 'mailing_view_all', 'mailing_delete', 'books'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['name'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Роль',
            'client_wh_create' => 'Создание',
            'client_wh_update' => 'Изменение',
            'client_wh_view' => 'Просмотр',
            'client_wh_view_all' => 'Просмотр всех',
            'client_wh_delete' => 'Удаление',
            'client_tg_create' => 'Создание',
            'client_tg_update' => 'Изменение',
            'client_tg_view' => 'Просмотр',
            'client_tg_view_all' => 'Просмотр всех',
            'client_tg_delete' => 'Удаление',
            'message_create' => 'Создание',
            'message_update' => 'Изменение',
            'message_view' => 'Просмотр',
            'message_view_all' => 'Просмотр всех',
            'message_delete' => 'Удаление',
            'recalls_create' => 'Создание',
            'recalls_update' => 'Изменение',
            'recalls_view' => 'Просмотр',
            'recalls_view_all' => 'Просмотр всех',
            'recalls_delete' => 'Удаление',
            'mailing_create' => 'Создание',
            'mailing_update' => 'Изменение',
            'mailing_view' => 'Просмотр',
            'mailing_view_all' => 'Просмотр всех',
            'mailing_delete' => 'Удаление',
            'books' => 'Справочники'
];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['role_id' => 'id']);
    }
}
