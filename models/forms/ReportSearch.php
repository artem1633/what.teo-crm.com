<?php 
namespace app\models\forms;


use app\models\ClientWh; 
use app\models\ClientTg; 
use app\models\Message; 
use app\models\Recalls; 
use app\models\Mailing; 
  

use app\models\ReportColumn;
use app\components\ComponentReport;
use Codeception\PHPUnit\ResultPrinter\Report;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * Class ReportSearch
 * @package app\models\forms
 */
class ReportSearch extends Model
{
    /**
     * @var array
     */
    public $setting;
    public $oldSetting;
    public $client_wh_name;
    public $client_wh_phone;
    public $client_wh_create_at;
    public $client_wh_tag;
    public $client_tg_name;
    public $client_tg_phone;
    public $client_tg_create_at;
    public $client_tg_step;
    public $client_tg_other;
    public $client_tg_wh;
    public $message_client_wh_id;
    public $message_text;
    public $message_create_at;
    public $recalls_client_wh_id;
    public $recalls_type;
    public $recalls_text;
    public $recalls_create_at;
    public $recalls_who;
    public $mailing_name;
    public $mailing_text;
    public $mailing_status;
  
    

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_wh_name','client_wh_phone','client_wh_create_at','client_wh_tag','client_tg_name','client_tg_phone','client_tg_create_at','client_tg_step','client_tg_other','client_tg_wh','message_client_wh_id','message_text','message_create_at','recalls_client_wh_id','recalls_type','recalls_text','recalls_create_at','recalls_who','mailing_name','mailing_text','mailing_status', 
    ], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'setting' => 'Колонки',
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = (new Query())->from('drivers');


        $dataProvider = new ArrayDataProvider([
            'allModels' => $query->all(),
        ]);





        return $dataProvider;
    }

    public static function fieldLabels()
    {
        return [

        ];
    }

}