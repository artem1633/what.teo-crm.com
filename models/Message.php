<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
* This is the model class for table "message".
*
    * @property int $client_wh_id Пользователь
    * @property  $text Текст
    * @property  $create_at Дата
*/
class Message extends \yii\db\ActiveRecord
{

        

    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'message';
    }

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['client_wh_id'], 'integer'],
            [['text', 'create_at'], 'string'],
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'client_wh_id' => 'Пользователь',
            'text' => 'Текст',
            'create_at' => 'Дата',
        ];
    }

    

    /**
    * {@inheritdoc}
    */
    public function beforeSave($insert) {



    return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {

    }


    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getClientWh()
    {
        return $this->hasOne(ClientWh::className(), ['id' => 'client_wh_id']);
    }

    
    

}