<?php 
namespace app\models;

use Yii;

/**
 * This is the model class for table "role".
 *
 * @property int $id
 * @property string $name Название
 * @property integer $client_wh_name Пвотцап Имя
 * @property integer $client_wh_phone Пвотцап Телефон
 * @property integer $client_wh_create_at Пвотцап Создан
 * @property integer $client_wh_tag Пвотцап Хэш тэк
 * @property integer $client_tg_name Птелеграмм Имя
 * @property integer $client_tg_phone Птелеграмм Телеграмм id
 * @property integer $client_tg_create_at Птелеграмм Создан
 * @property integer $client_tg_step Птелеграмм step
 * @property integer $client_tg_other Птелеграмм other
 * @property integer $client_tg_wh Птелеграмм wh
 * @property integer $message_client_wh_id Сообщения Пользователь
 * @property integer $message_text Сообщения Текст
 * @property integer $message_create_at Сообщения Дата
 * @property integer $recalls_client_wh_id Отзовы Пользователь
 * @property integer $recalls_type Отзовы Тип
 * @property integer $recalls_text Отзовы Текст
 * @property integer $recalls_create_at Отзовы Создан
 * @property integer $recalls_who Отзовы Кто написал
 * @property integer $mailing_name Рассылка Наименование
 * @property integer $mailing_text Рассылка Текст
 * @property integer $mailing_status Рассылка Статус
 * @property integer $books Справочники
 *
 * @property User[] $users
 */
class ReportColumn extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report_column';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_wh_name', 'client_wh_phone', 'client_wh_create_at', 'client_wh_tag', 'client_tg_name', 'client_tg_phone', 'client_tg_create_at', 'client_tg_step', 'client_tg_other', 'client_tg_wh', 'message_client_wh_id', 'message_text', 'message_create_at', 'recalls_client_wh_id', 'recalls_type', 'recalls_text', 'recalls_create_at', 'recalls_who', 'mailing_name', 'mailing_text', 'mailing_status'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['name'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'client_wh_name' => 'Имя',
            'client_wh_phone' => 'Телефон',
            'client_wh_create_at' => 'Создан',
            'client_wh_tag' => 'Хэш тэк',
            'client_tg_name' => 'Имя',
            'client_tg_phone' => 'Телеграмм id',
            'client_tg_create_at' => 'Создан',
            'client_tg_step' => 'step',
            'client_tg_other' => 'other',
            'client_tg_wh' => 'wh',
            'message_client_wh_id' => 'Пользователь',
            'message_text' => 'Текст',
            'message_create_at' => 'Дата',
            'recalls_client_wh_id' => 'Пользователь',
            'recalls_type' => 'Тип',
            'recalls_text' => 'Текст',
            'recalls_create_at' => 'Создан',
            'recalls_who' => 'Кто написал',
            'mailing_name' => 'Наименование',
            'mailing_text' => 'Текст',
            'mailing_status' => 'Статус',
            
        ];
    }


    /**
     * @return array
     */
    public function getColumnsAttributes(){
        return [
     'client_wh_name',
     'client_wh_phone',
     'client_wh_create_at',
     'client_wh_tag',
     'client_tg_name',
     'client_tg_phone',
     'client_tg_create_at',
     'client_tg_step',
     'client_tg_other',
     'client_tg_wh',
     'message_client_wh_id',
     'message_text',
     'message_create_at',
     'recalls_client_wh_id',
     'recalls_type',
     'recalls_text',
     'recalls_create_at',
     'recalls_who',
     'mailing_name',
     'mailing_text',
     'mailing_status',
    
        ];
    }


    /**
     * @inheritdoc
     */
    public function getColumns()
    {
        $columns = [];


        foreach ($this->getColumnsAttributes() as $attr){
            if($this->$attr != null){
                $arr = explode('_', $attr);
  
                if($arr[0] == 'client_wh'){
                    $str = 'client_wh_'.implode('_', array_slice($arr, 1));
                    $header = '<span class="label label-primary">Пвотцап</span>';
                }
  
                if($arr[0] == 'client_tg'){
                    $str = 'client_tg_'.implode('_', array_slice($arr, 1));
                    $header = '<span class="label label-primary">Птелеграмм</span>';
                }
  
                if($arr[0] == 'message'){
                    $str = 'message_'.implode('_', array_slice($arr, 1));
                    $header = '<span class="label label-primary">Сообщения</span>';
                }
  
                if($arr[0] == 'recalls'){
                    $str = 'recalls_'.implode('_', array_slice($arr, 1));
                    $header = '<span class="label label-primary">Отзовы</span>';
                }
  
                if($arr[0] == 'mailing'){
                    $str = 'mailing_'.implode('_', array_slice($arr, 1));
                    $header = '<span class="label label-primary">Рассылка</span>';
                }
  
                $columns[] = [
                    'attribute' => $str,
                    'header' => "<div style='width: 100%; text-align: center;'>{$header}</div>".$this->getAttributeLabel($attr),
                ];
            }
        }

        return $columns;
    }

}
