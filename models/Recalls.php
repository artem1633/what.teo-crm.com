<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
* This is the model class for table "recalls".
*
    * @property int $client_wh_id Пользователь
    * @property  $type Тип
    * @property  $text Текст
    * @property  $create_at Создан
    * @property int $who Кто написал
*/
class Recalls extends \yii\db\ActiveRecord
{

        const UP = 'Положительный';
    const DOWN = 'Отрицательный';
    

    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'recalls';
    }

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['client_wh_id', 'who'], 'integer'],
            [['type', 'text', 'create_at'], 'string'],
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'client_wh_id' => 'Пользователь',
            'type' => 'Тип',
            'text' => 'Текст',
            'create_at' => 'Создан',
            'who' => 'Кто написал',
        ];
    }

    public static function typeLabels() {
        return [
            self::UP => "Положительный",
            self::DOWN => "Отрицательный",
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function beforeSave($insert) {



    return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {

    }


    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getClientWh()
    {
        return $this->hasOne(ClientWh::className(), ['id' => 'client_wh_id']);
    }

    
    
    
    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getWho()
    {
        return $this->hasOne(ClientTg::className(), ['id' => 'who']);
    }


}