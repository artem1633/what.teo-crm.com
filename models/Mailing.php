<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
* This is the model class for table "mailing".
*
    * @property string $name Наименование
    * @property  $text Текст
    * @property  $status Статус
*/
class Mailing extends \yii\db\ActiveRecord
{

        const NEW = 'Новая';
    const CLOUSE = 'Выподнена';
    

    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'mailing';
    }

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['name', 'text', 'status'], 'required'],
            [['name', 'text', 'status'], 'string'],
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'name' => 'Наименование',
            'text' => 'Текст',
            'status' => 'Статус',
        ];
    }

    public static function statusLabels() {
        return [
            self::NEW => "Новая",
            self::CLOUSE => "Выподнена",
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function beforeSave($insert) {



    return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {

    }


    
    
    

}