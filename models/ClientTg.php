<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
* This is the model class for table "client_tg".
*
    * @property string $name Имя
    * @property string $phone Телеграмм id
    * @property  $create_at Создан
    * @property string $step step
    * @property string $other other
    * @property string $wh wh
*/
class ClientTg extends \yii\db\ActiveRecord
{

        

    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'client_tg';
    }

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['name', 'phone', 'create_at', 'step', 'other', 'wh'], 'string'],
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'phone' => 'Телеграмм id',
            'create_at' => 'Создан',
            'step' => 'step',
            'other' => 'other',
            'wh' => 'wh',
        ];
    }

    

    /**
    * {@inheritdoc}
    */
    public function beforeSave($insert) {



    return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {

    }


    
    
    
    
    
    
    

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getRecallss()
    {
        return $this->hasMany(Recallss::className(), ['who' => 'id']);
    }

}