<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m20210130_175451_create_group_1_table`.
 */
class m20210130_175451_create_group_1_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('group_1', [
            'id' => $this->primaryKey(),
                                        'name' => $this->string()->comment('Наименование'),
                
                
                    ]);

        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        
        $this->dropTable('group_1');

    }
}
