<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m20210130_175633_create_group_2_table`.
 */
class m20210130_175633_create_group_2_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('group_2', [
            'id' => $this->primaryKey(),
                                        'name' => $this->string()->comment('Наименование'),
                
                
                                        'group1_id' => $this->integer()->comment('Связь с группой 1'),
                
                
                    ]);

        $this->createIndex(
                            'idx-group_2-group1_id',
                            'group_2',
                            'group1_id'
                        );
                        
                        $this->addForeignKey(
                            'fk-group_2-group1_id',
                            'group_2',
                            'group1_id',
                            'group_1',
                            'id',
                            'SET NULL'
                        );
                        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        
                        $this->dropForeignKey(
                            'fk-group_2-group1_id',
                            'group_2'
                        );
                        
                        $this->dropIndex(
                            'idx-group_2-group1_id',
                            'group_2'
                        );
                        
                        
        $this->dropTable('group_2');

    }
}
