<?php

use app\admintheme\widgets\Menu;


?>



<div id="sidebar" class="sidebar">
    <?php if (Yii::$app->user->isGuest == false): ?>        <?php        try {
            echo Menu::widget(
                [
                    'options' => ['class' => 'nav'],
                    'items' => [
                                                    ['label' => 'Группа 2', 'icon' => 'fa  fa-user', 'url' => ['/group-2']],
                                                    ['label' => 'Группа 1', 'icon' => 'fa  fa-user', 'url' => ['/group-1']],
                                            ],
                ]
            );
        } catch (Exception $e) {
            Yii::error($e->getMessage(), '_error');
            echo $e->getMessage();
        }
        ?>
    <?php endif; ?>
</div>
