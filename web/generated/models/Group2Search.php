<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use Group2;

/**
 * Group2Search represents the model behind the search form about `Group2`.
 */
class Group2Search extends Group2
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group1_id'], 'exist', 'skipOnError' => true, 'targetClass' => Group1::className(), 'targetAttribute' => ['group1_id' => 'id']],
            [['name'], 'string'],
            [['group1_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Group2::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'name' => $this->name,
            'group1_id' => $this->group1_id,
        ]);

        return $dataProvider;
    }
}
