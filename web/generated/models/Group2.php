<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "group_2".
*
    * @property string $name Наименование
    * @property int $group1_id Связь с группой 1
*/
class Group2 extends \yii\db\ActiveRecord
{
/**
* {@inheritdoc}
*/
public static function tableName()
{
return 'group_2';
}

/**
* {@inheritdoc}
*/
public function rules()
{
return [
            [['group1_id'], 'exist', 'skipOnError' => true, 'targetClass' => Group1::className(), 'targetAttribute' => ['group1_id' => 'id']],
            [['name', 'group1_id'], 'required'],
            [['name'], 'string'],
            [['group1_id'], 'integer'],
        ];
}

/**
* {@inheritdoc}
*/
public function attributeLabels()
{
return [
    'name' => 'Наименование',
    'group1_id' => 'Связь с группой 1',
];
}

    
    
    
    
        
        /**
        * @return \yii\db\ActiveQuery
        */
        public function getGroup1()
        {
        return $this->hasOne(Group1::className(), ['id' => 'group1_id']);
        }

    }
