<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "group_1".
*
    * @property string $name Наименование
*/
class Group1 extends \yii\db\ActiveRecord
{
/**
* {@inheritdoc}
*/
public static function tableName()
{
return 'group_1';
}

/**
* {@inheritdoc}
*/
public function rules()
{
return [
            [['name'], 'required'],
            [['name'], 'string'],
        ];
}

/**
* {@inheritdoc}
*/
public function attributeLabels()
{
return [
    'name' => 'Наименование',
];
}

    }
