<?php

namespace app\controllers;

use Yii;
use app\models\Group1;
    use app\models\Group1Search;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
* Group1Controller implements the CRUD actions for Group1 model.
*/
class Group1Controller extends Controller
{
/**
* @inheritdoc
*/
public function behaviors()
{
return [
'verbs' => [
'class' => VerbFilter::className(),
'actions' => [
'delete' => ['post'],
'bulk-delete' => ['post'],
],
],
];
}

/**
* Lists all Group1 models.
* @return mixed
*/
public function actionIndex()
{
    $searchModel = new Group1Search();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
    'searchModel' => $searchModel,
    'dataProvider' => $dataProvider,
    ]);
}


/**
* Displays a single Group1 model.
* 
* @return mixed
*/
public function actionView($id)
{
$request = Yii::$app->request;
if($request->isAjax){
Yii::$app->response->format = Response::FORMAT_JSON;
return [
'title'=> "Group1 #".$id,
'content'=>$this->renderAjax('view', [
'model' => $this->findModel($id),
]),
'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
Html::a('Edit',['update','id'=>$model->id],['class'=>'btn btn-primary','role'=>'modal-remote'])
];
}else{
return $this->render('view', [
'model' => $this->findModel($model->id),
]);
}
}

/**
* Creates a new Group1 model.
* For ajax request will return json object
* and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
* @return mixed
*/
public function actionCreate()
{
$request = Yii::$app->request;
$model = new Group1();

if($request->isAjax){
/*
*   Process for ajax request
*/
Yii::$app->response->format = Response::FORMAT_JSON;
if($request->isGet){
return [
'title'=> "Create new Group1",
'content'=>$this->renderAjax('create', [
'model' => $model,
]),
'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

];
}else if($model->load($request->post()) && $model->save()){
return [
'forceReload'=>'#crud-datatable-pjax',
'title'=> "Create new Group1",
'content'=>'<span class="text-success">Create Group1 success</span>',
'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
Html::a('Create More',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])

];
}else{
return [
'title'=> "Create new Group1",
'content'=>$this->renderAjax('create', [
'model' => $model,
]),
'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

];
}
}else{
/*
*   Process for non-ajax request
*/
if ($model->load($request->post()) && $model->save()) {
return $this->redirect(['view', 'id' => $model->id]);
} else {
return $this->render('create', [
'model' => $model,
]);
}
}

}

/**
* Updates an existing Group1 model.
* For ajax request will return json object
* and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
* 
* @return mixed
*/
public function actionUpdate($id)
{
$request = Yii::$app->request;
$model = $this->findModel($id);

if($request->isAjax){
/*
*   Process for ajax request
*/
Yii::$app->response->format = Response::FORMAT_JSON;
if($request->isGet){
return [
'title'=> "Update Group1 #".$model->id,
'content'=>$this->renderAjax('update', [
'model' => $model,
]),
'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
];
}else if($model->load($request->post()) && $model->save()){
return [
'forceReload'=>'#crud-datatable-pjax',
'title'=> "Group1 #".$model->id,
'content'=>$this->renderAjax('view', [
'model' => $model,
]),
'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
Html::a('Edit',['update','id'=>$model->id],['class'=>'btn btn-primary','role'=>'modal-remote'])
];
}else{
return [
'title'=> "Update Group1 #".$model->id,
'content'=>$this->renderAjax('update', [
'model' => $model,
]),
'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
];
}
}else{
/*
*   Process for non-ajax request
*/
if ($model->load($request->post()) && $model->save()) {
return $this->redirect(['view', 'id' => $model->id]);
} else {
return $this->render('update', [
'model' => $model,
]);
}
}
}

/**
* Delete an existing Group1 model.
* For ajax request will return json object
* and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
* 
* @return mixed
*/
public function actionDelete($id)
{
$request = Yii::$app->request;
$this->findModel($model->id)->delete();

if($request->isAjax){
/*
*   Process for ajax request
*/
Yii::$app->response->format = Response::FORMAT_JSON;
return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
}else{
/*
*   Process for non-ajax request
*/
return $this->redirect(['index']);
}


}

/**
* Delete multiple existing Group1 model.
* For ajax request will return json object
* and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
* 
* @return mixed
*/
public function actionBulkDelete()
{
$request = Yii::$app->request;
$pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
foreach ( $pks as $pk ) {
$model = $this->findModel($pk);
$model->delete();
}

if($request->isAjax){
/*
*   Process for ajax request
*/
Yii::$app->response->format = Response::FORMAT_JSON;
return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
}else{
/*
*   Process for non-ajax request
*/
return $this->redirect(['index']);
}

}

/**
* Finds the Group1 model based on its primary key value.
* If the model is not found, a 404 HTTP exception will be thrown.
* 
* @return Group1 the loaded model
* @throws NotFoundHttpException if the model cannot be found
*/
protected function findModel($id)
{
if (($model = Group1::findOne($id)) !== null) {
return $model;
} else {
throw new NotFoundHttpException('The requested page does not exist.');
}
}
}
