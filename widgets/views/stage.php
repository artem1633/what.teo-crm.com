<?php

use yii\helpers\Html;
use \app\widgets\QuestionSortableItem;
use \kartik\sortable\Sortable;

/**
 * @var $model \app\models\ApplicationFormStage
 * @var $questions \app\models\Question[]
 */

$items = [];
foreach ($questions as $question)
{
    $items[] = ['content' => QuestionSortableItem::widget(['question' => $question])];
}

?>


<div class="stage-id" data-id="<?=$model->id?>">
    <h4><i class="fa fa-list"></i> <?= $model->name_ru ?></h4>
    <?= Html::a('<i class="fa fa-pencil"></i>', ['application-form-stage/update', 'id' => $model->id], ['role' => 'modal-remote', 'class' => 'pull-right', 'style' => 'margin-top: -37px;']) ?>
</div>
<?= Sortable::widget([
    'id'=>'stage-'.$model->id,
    'options' => [
        'data-stage' => $model->id,
    ],
    'items' => $items,
    'connected' => true,
    'pluginEvents' => [
        'sortupdate' => 'function(e, e2){
                    
//                    var ordering = e2.item.index();

                    
                    var sortingArray = [];
                    
                    $(e.target).find("li").each(function(){
                        var questionId = $(this).find(".question-sort").data("id");
                        sortingArray.push(questionId);
                    });
                    
                    if(sortingArray.length == 0){
                        return;
                    }
                    
                    console.log(sortingArray);
                    
                    csrfParam = yii.getCsrfParam();
                    
                    $.ajax({
                        url: "/question/sorting",
                        method: "POST",
                        data: {
                            data: JSON.stringify(sortingArray),
                            csrfParam: yii.getCsrfToken(),
                        },
                        success: function(response){
                            console.log("response", response);
                        },
                    });
                }'
    ],
]) ?>