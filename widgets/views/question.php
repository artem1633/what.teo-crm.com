<?php

use yii\helpers\Html;

/**
 * @var $model \app\models\Question
 */

?>

<div class="question-sort" data-id="<?=$model->id?>">
    <?= Html::a('<i class="fa fa-pencil"></i>', ['question/update', 'id' => $model->id], ['role' => 'modal-remote', 'class' => 'pull-right']) ?>
    <?= Html::a('<i class="fa fa-trash"></i>', ['question/delete', 'id' => $model->id], ['role' => 'modal-remote', 'class' => 'pull-right',
        'title'=>'Удалить',
        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
        'data-request-method'=>'post',
        'data-confirm-title'=>'Вы уверены?',
        'data-confirm-message'=>'Вы действительно хотите удалить данную запись?']) ?>

    <?php if($model->type == \app\models\Question::TYPE_TEXT): ?>

        <h5><i class="fa fa-edit"></i> <?= $model->name_ru ?></h5>
        <input class="form-control" type="text" disabled>

    <?php elseif($model->type == \app\models\Question::TYPE_BOOK): ?>

        <h5><i class="fa fa-edit"></i> <?= $model->name_ru ?></h5>
        <input class="form-control" type="text" disabled>

    <?php elseif($model->type == \app\models\Question::TYPE_DATETIME): ?>

        <h5><i class="fa fa-clock-o"></i> <?= $model->name_ru ?></h5>
        <input class="form-control" type="text" disabled>

    <?php elseif($model->type == \app\models\Question::TYPE_DATE): ?>

        <h5><i class="fa fa-calendar"></i> <?= $model->name_ru ?></h5>
        <input class="form-control" type="text" disabled>

    <?php endif; ?>
</div>


