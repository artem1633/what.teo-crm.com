<?php

namespace app\widgets;

use app\models\Question;
use yii\base\Widget;
use yii\web\NotFoundHttpException;

/**
 * Class QuestionSortableItem
 * @package app\widgets
 */
class QuestionSortableItem extends Widget
{
    /**
     * @var Question
     */
    public $question;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if($this->question == null){
            throw new NotFoundHttpException('Question must be required');
        }
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        parent::run();

        return $this->render('question', [
            'model' => $this->question,
        ]);
    }
}