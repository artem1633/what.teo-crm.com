<?php

namespace app\widgets;

use app\models\ApplicationFormStage;
use app\models\Question;
use yii\base\Widget;
use yii\web\NotFoundHttpException;

/**
 * Class StageSortableItem
 * @package app\widgets
 */
class StageSortableItem extends Widget
{
    /**
     * @var ApplicationFormStage
     */
    public $stage;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if($this->stage == null){
            throw new NotFoundHttpException('Stage must be required');
        }
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        parent::run();

        $questions = Question::find()->where(['stage_id' => $this->stage->id])->orderBy('ordering asc')->all();

        return $this->render('stage', [
            'model' => $this->stage,
            'questions' => $questions,
        ]);
    }
}