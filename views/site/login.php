<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
///* @var $model \common\models\LoginForm */

$this->title = 'Authorization';

$fieldOptions1 = [
    'options' => ['class' => 'form-group m-b-20'],
    'inputTemplate' => "{input}"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group m-b-20'],
    'inputTemplate' => "{input}"
];

?>
<div class="login bg-black animated fadeInDown">
    <!-- begin brand -->
    <div class="login-header">
        <div class="brand">
            <span class="logo"></span> <?=Yii::$app->name ?>            <small>Enter authorization information</small>
        </div>
        <div class="icon">
            <i class="fa fa-sign-in"></i>
        </div>
    </div>
    <!-- end brand -->
    <div class="login-content">
        <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false], $options = ['class' => 'margin-bottom-0']); ?>        <?= $form
            ->field($model, 'username', $fieldOptions1)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('email'), 'class' => 'form-control input-lg inverse-mode no-border']) ?>        <?= $form
            ->field($model, 'password', $fieldOptions2)
            ->label(false)
            ->passwordInput(['placeholder' => $model->getAttributeLabel('password'), 'class' => 'form-control input-lg inverse-mode no-border']) ?>        <div class="login-buttons">
            <?= Html::submitButton('Enter', ['class' => 'btn btn-success btn-block btn-lg', 'name' => 'login-button']) ?>        </div>
        <div style="text-align: center; margin-top: 22px;">
            <?= Html::a('Forgot password', ['site/forget-password']) ?>        </div>
        <?php ActiveForm::end(); ?>    </div>
</div>

