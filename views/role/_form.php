<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Role */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="role-form">

    <?php $form = ActiveForm::begin(); ?>

        <div class="row">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
            <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <h5 style="color:#16508A;">Пвотцап</h5>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'client_wh_create')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'client_wh_update')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'client_wh_delete')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'client_wh_view')->checkbox()?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'client_wh_view_all')->checkbox() ?>
                </div>
            </div>
        </div>
            <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <h5 style="color:#16508A;">Птелеграмм</h5>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'client_tg_create')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'client_tg_update')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'client_tg_delete')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'client_tg_view')->checkbox()?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'client_tg_view_all')->checkbox() ?>
                </div>
            </div>
        </div>
            <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <h5 style="color:#16508A;">Сообщения</h5>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'message_create')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'message_update')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'message_delete')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'message_view')->checkbox()?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'message_view_all')->checkbox() ?>
                </div>
            </div>
        </div>
            <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <h5 style="color:#16508A;">Отзовы</h5>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'recalls_create')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'recalls_update')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'recalls_delete')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'recalls_view')->checkbox()?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'recalls_view_all')->checkbox() ?>
                </div>
            </div>
        </div>
            <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <h5 style="color:#16508A;">Рассылка</h5>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'mailing_create')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'mailing_update')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'mailing_delete')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'mailing_view')->checkbox()?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'mailing_view_all')->checkbox() ?>
                </div>
            </div>
        </div>
    

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'books')->checkbox() ?>
        </div>
    </div>

    <?php  if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?=  Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php  } ?>

    <?php  ActiveForm::end(); ?>
</div>
