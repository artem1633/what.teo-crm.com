<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Recalls */
/* @var $form yii\widgets\ActiveForm */

if($model->isNewRecord == false){
}
?>

<div class="recalls-form">

    <?php $form = ActiveForm::begin(); ?>

<div class="row">


    
            

            <div class="col-md-12">

                    
                 <?= $form->field($model, 'client_wh_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\ClientWh::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ]) ?>
        

    

</div>

    
            

            <div class="col-md-12">

    
        
                 <?= $form->field($model, 'type')->dropDownList(app\models\Recalls::typeLabels()) ?>
         

    

</div>

    
            

            <div class="col-md-12">

    
                 <?= $form->field($model, 'text')->textarea()  ?>
        

    

</div>

    
            

            <div class="col-md-12">

                                <?= $form->field($model, 'create_at')->textInput() ?>
         
    

</div>

    
            

            <div class="col-md-12">

                    
                 <?= $form->field($model, 'who')->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\ClientTg::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ]) ?>
        

    

</div>
</div>


	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
