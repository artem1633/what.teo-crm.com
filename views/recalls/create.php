<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model Recalls */

?>
<div class="recalls-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
