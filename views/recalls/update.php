<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model Recalls */
?>
<div class="recalls-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
