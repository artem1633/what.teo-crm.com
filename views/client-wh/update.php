<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model ClientWh */
?>
<div class="client-wh-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
