<?php

use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model ClientWh */
?>
<div class="client-wh-view">
 

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Информация</h4>
                    <div class="panel-heading-btn" style="margin-top: -20px;">
                        <a class="btn btn-warning btn-xs" href="/client-wh/update?id=<?= $model->id?>&amp;containerPjaxReload=%23pjax-container-info-container" role="modal-remote"><span class="glyphicon glyphicon-pencil"></span></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </div>
                <div class="panel-body" style="display: none;">
                    <div class="row">
                        <div class="col-md-12">

                            <?= DetailView::widget([
                                'model' => $model,
                                'attributes' => [
                                    'name',
            'phone',
            'create_at',
            'tag',
                                ],
                            ]) ?>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>




    
            
        
   <?= $this->render("@app/views/message/index.php", [
            'searchModel' => $messageSearchModel,
            'dataProvider' => $messageDataProvider,
            'additionalLinkParams' => ['Message[client_wh_id]' => $model->id]
        ]); ?>
            
        
   <?= $this->render("@app/views/recalls/index.php", [
            'searchModel' => $recallsSearchModel,
            'dataProvider' => $recallsDataProvider,
            'additionalLinkParams' => ['Recalls[client_wh_id]' => $model->id]
        ]); ?>
    

</div>
