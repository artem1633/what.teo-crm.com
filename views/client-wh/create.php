<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model ClientWh */

?>
<div class="client-wh-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
