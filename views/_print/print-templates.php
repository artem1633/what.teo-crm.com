<?php

use yii\helpers\Html;

/** @var $templates \app\models\Template */
/** @var $id int */

?>

<?php foreach ($templates as $template): ?>

    <?= Html::a($template->name, ['print-template', 'id' => $id, 'template_id' => $template->id], ['class' => 'btn btn-success btn-block', 'target' => '_blank']); ?>

<?php endforeach; ?>
