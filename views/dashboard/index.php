<?php


use yii\helpers\Html;

?>

<div class="panel panel-inverse">
    <div class="panel-heading">
        <h4 class="panel-title"></h4>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6">
                <?= Html::a('Сегодня', ['#'], ['class' => 'btn btn-success']) ?>
                <?= Html::a('Вчера', ['#'], ['class' => 'btn btn-default']) ?>
                <?= Html::a('Неделя', ['#'], ['class' => 'btn btn-default']) ?>
                <?= Html::a('Пред. неделя', ['#'], ['class' => 'btn btn-default']) ?>
                <?= Html::a('Месяц', ['#'], ['class' => 'btn btn-default']) ?>
                <?= Html::a('Пред. месяц', ['#'], ['class' => 'btn btn-default']) ?>
            </div>
            <div class="col-md-2">
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-8">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Обороты денежных средств</h4>
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-sm btn-icon btn-circle btn-default" data-original-title="" title="" data-init="true" style="margin-top: -38px;"><i class="fa fa-cog"></i></a>
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th scope="col"></th>
                        <th scope="col">Банк</th>
                        <th scope="col">Касса</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row"><b>Приход</b></th>
                        <td class="text-success"><b>+0,00</b></td>
                        <td class="text-success"><b>+0,00</b></td>
                    </tr>
                    <tr>
                        <th scope="row"><b>Расход</b></th>
                        <td class="text-warning"><b>+0,00</b></td>
                        <td class="text-warning"><b>+0,00</b></td>
                    </tr>
                    <tr>
                        <th scope="row"><b>Остаток</b></th>
                        <td><b>25 459,00</b></td>
                        <td><b>308 126,00</b></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Остатки денежных средств</h4>
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-sm btn-icon btn-circle btn-default" data-original-title="" title="" data-init="true" style="margin-top: -38px;"><i class="fa fa-cog"></i></a>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <p style="font-size: 14px; margin-bottom: 5px; margin-top: 0;">Банк</p>
                        <b style="font-size: 22px;">25 945,00</b>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="font-size: 14px; margin-bottom: 5px; margin-top: 5px;">Касса</p>
                        <b style="font-size: 22px;">23 688,00</b>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="font-size: 14px; margin-bottom: 5px; margin-top: 5px;">Общие итоги</p>
                        <b style="font-size: 22px;">33 901,00</b>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-8">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <h4 class="panel-title">Показатели по менеджерам</h4>
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-sm btn-icon btn-circle btn-default" data-original-title="" title="" data-init="true" style="margin-top: -38px;"><i class="fa fa-cog"></i></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <canvas id="charContent" width="400" height="100%"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <h4 class="panel-title">Задолжность</h4>
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-sm btn-icon btn-circle btn-default" data-original-title="" title="" data-init="true" style="margin-top: -38px;"><i class="fa fa-cog"></i></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th scope="col">Контрагент</th>
                                <th scope="col">Задолжность</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>ООО «Вкусные продукты»</td>
                                <td>1 894,00</td>
                            </tr>
                            <tr>
                                <td>Всего</td>
                                <td>1 894,00</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <h4 class="panel-title">Показатели компании</h4>
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-sm btn-icon btn-circle btn-default" data-original-title="" title="" data-init="true" style="margin-top: -38px;"><i class="fa fa-cog"></i></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <canvas id="charContent2" width="400" height="100%"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <h4 class="panel-title">Диаграмма по менеджерам</h4>
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-sm btn-icon btn-circle btn-default" data-original-title="" title="" data-init="true" style="margin-top: -38px;"><i class="fa fa-cog"></i></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <canvas id="charContent3" width="400" height="350%"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.min.js"></script>

<script>
    var ctx = document.getElementById('charContent').getContext('2d');
    var charContent = new Chart(ctx, {
        type: 'line',
        data: {
            labels: ['01-Март', '02-Март', '03-Март', '04-Март', '05-Март', '06-Март', '07-Март'],
            datasets: [{
                label: 'Принято заказов',
                data: [120, 190, 300, 170, 600, 340, 450],
                backgroundColor: "rgba(23, 191, 205,0.6)"
            }]
        }
    });
    var ctx = document.getElementById('charContent2').getContext('2d');
    var charContent = new Chart(ctx, {
        type: 'line',
        data: {
            labels: ['01-Март', '02-Март', '03-Март', '04-Март', '05-Март', '06-Март', '07-Март'],
            datasets: [{
                label: 'Прибыль',
                data: [120, 600, 190, 300, 170, 340, 450],
                backgroundColor: "rgba(23, 191, 205,0.6)"
            }, {
                label: 'Сумма заказов',
                data: [190, 120, 600, 300, 340, 450, 170],
                backgroundColor: "rgba(245, 156, 26,0.6)"
            }]
        }
    });
    var ctx = document.getElementById('charContent3').getContext('2d');
    var charContent = new Chart(ctx, {
        type: 'line',
        data: {
            labels: ['01-Март', '02-Март', '03-Март', '04-Март', '05-Март', '06-Март', '07-Март'],
            datasets: [{
                label: 'Менеджер',
                data: [120, 600, 170, 190, 300, 340, 450],
                backgroundColor: "rgba(23, 191, 205,0.6)"
            }]
        }
    });
</script>