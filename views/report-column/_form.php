<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Role */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="role-form">

    <?php $form = ActiveForm::begin(); ?>

        <div class="row">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="row">
            <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <h5 style="color:#16508A;">Пвотцап</h5>
                </div>
            </div>
            <div class="row">

                <div class="col-md-12">
                    <?= $form->field($model, 'client_wh_name')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'client_wh_phone')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'client_wh_create_at')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'client_wh_tag')->checkbox() ?>
                </div>

            </div>
        </div>
            <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <h5 style="color:#16508A;">Птелеграмм</h5>
                </div>
            </div>
            <div class="row">

                <div class="col-md-12">
                    <?= $form->field($model, 'client_tg_name')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'client_tg_phone')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'client_tg_create_at')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'client_tg_step')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'client_tg_other')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'client_tg_wh')->checkbox() ?>
                </div>

            </div>
        </div>
            <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <h5 style="color:#16508A;">Сообщения</h5>
                </div>
            </div>
            <div class="row">

                <div class="col-md-12">
                    <?= $form->field($model, 'message_client_wh_id')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'message_text')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'message_create_at')->checkbox() ?>
                </div>

            </div>
        </div>
            <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <h5 style="color:#16508A;">Отзовы</h5>
                </div>
            </div>
            <div class="row">

                <div class="col-md-12">
                    <?= $form->field($model, 'recalls_client_wh_id')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'recalls_type')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'recalls_text')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'recalls_create_at')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'recalls_who')->checkbox() ?>
                </div>

            </div>
        </div>
            <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <h5 style="color:#16508A;">Рассылка</h5>
                </div>
            </div>
            <div class="row">

                <div class="col-md-12">
                    <?= $form->field($model, 'mailing_name')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'mailing_text')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'mailing_status')->checkbox() ?>
                </div>

            </div>
        </div>
    </div>

    <?php  if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?=  Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php  } ?>

    <?php  ActiveForm::end(); ?>
</div>
