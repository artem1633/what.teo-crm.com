<?php

use app\admintheme\widgets\Menu;


?>



<div id="sidebar" class="sidebar sidebar-grid">
    <?php if (Yii::$app->user->isGuest == false): ?>        <?php        try {
            echo Menu::widget(
                [
                    'options' => ['class' => 'nav'],
                    'items' => [
                        ['label' => 'Рабочий стол', 'icon' => 'fa fa-bar-chart', 'url' => ['/dashboard']],
                        ['label' => 'Пвотцап', 'icon' => 'fa fa-angellist', 'url' => ['/client-wh'], 'visible' => Yii::$app->user->identity->can('client_wh_view')],
                        ['label' => 'Птелеграмм', 'icon' => 'fa fa-android', 'url' => ['/client-tg'], 'visible' => Yii::$app->user->identity->can('client_tg_view')],
                        ['label' => 'Рассылка', 'icon' => 'fa fa-at', 'url' => ['/mailing'], 'visible' => Yii::$app->user->identity->can('mailing_view')],

                    ['label' => 'Настройки', 'icon' => 'fa fa-list-ul', 'url' => '#', 'options' => ['class' => 'has-sub'], 'visible' => true,
                        'items' => [
                            ['label' => 'Пользователи', 'icon' => 'fa  fa-user-o', 'url' => ['/user']],
                            ['label' => 'Роли', 'icon' => 'fa  fa-star', 'url' => ['/role']],
                            ['label' => 'Отчеты', 'icon' => 'fa  fa-star', 'url' => ['/report']],
                            ['label' => 'Поля отчета', 'icon' => 'fa  fa-star', 'url' => ['/report-column']],
                                                        
                                                ],
                ],
                ],
                ]
            );
        } catch (Exception $e) {
            Yii::error($e->getMessage(), '_error');
            echo $e->getMessage();
        }
        ?>
    <?php endif; ?>
</div>
