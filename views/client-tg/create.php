<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model ClientTg */

?>
<div class="client-tg-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
