<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel MailingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Рассылка";
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

if(isset($additionalLinkParams)){
    $createUrl = ArrayHelper::merge(['mailing/create'], $additionalLinkParams);
} else {
    $createUrl = ['mailing/create'];
}

?>
<style>
    .modal-dialog {
        width: 80% !important;
    }
</style>
<div class="panel panel-inverse project-index">
    <div class="panel-heading">
        <!--        <div class="panel-heading-btn">-->
        <!--        </div>-->
        <h4 class="panel-title">Рассылка</h4>
    </div>
    <div class="panel-body">
<div class="mailing-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => require(__DIR__.'/_columns.php'),
                'panelBeforeTemplate' =>    Html::a('Добавить <i class="fa fa-plus"></i>', $createUrl,
                ['role'=>'modal-remote','title'=> 'Добавить','class'=>'btn btn-success']),
             
            'striped' => true,
            'condensed' => true,
            'responsive' => true,  
            'responsiveWrap' => false,        
            'panel' => [
                'headingOptions' => ['style' => 'display: none;'],
                'after'=>BulkButtonWidget::widget([
                            'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить все',
                                ["bulk-delete"] ,
                                [
                                    "class"=>"btn btn-danger btn-xs",
                                    'role'=>'modal-remote-bulk',
                                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                    'data-request-method'=>'post',
                                    'data-confirm-title'=>'Are you sure?',
                                    'data-confirm-message'=>'Вы действительно хотите удалить эту запись?'
                                ]),
                        ]).                        
                        '<div class="clearfix"></div>',
            ]
        ])?>
    </div>
</div>
    </div>
</div>

<?php
$script = <<< JS
$('[data-key]').click(function(e){
    if($(e.target).is('td')){
        var id = $(this).data('key');
        window.location = '/mailing/view?id='+id;
    }
});
JS;

$this->registerJs($script, \yii\web\View::POS_READY);
?>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
