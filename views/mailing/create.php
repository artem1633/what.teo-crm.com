<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model Mailing */

?>
<div class="mailing-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
