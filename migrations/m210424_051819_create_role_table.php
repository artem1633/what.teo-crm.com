<?php
use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m210424_051819_create_role_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('role', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'client_wh_create' => $this->boolean()->defaultValue(false)->comment('Пвотцап Добавление'),
            'client_wh_update' => $this->boolean()->defaultValue(false)->comment('Пвотцап Изменение'),
            'client_wh_view' => $this->boolean()->defaultValue(false)->comment('Пвотцап Просмотр'),
            'client_wh_view_all' => $this->boolean()->defaultValue(false)->comment('Пвотцап Просмотр всех'),
            'client_wh_delete' => $this->boolean()->defaultValue(false)->comment('Пвотцап Удалить'),
            'client_tg_create' => $this->boolean()->defaultValue(false)->comment('Птелеграмм Добавление'),
            'client_tg_update' => $this->boolean()->defaultValue(false)->comment('Птелеграмм Изменение'),
            'client_tg_view' => $this->boolean()->defaultValue(false)->comment('Птелеграмм Просмотр'),
            'client_tg_view_all' => $this->boolean()->defaultValue(false)->comment('Птелеграмм Просмотр всех'),
            'client_tg_delete' => $this->boolean()->defaultValue(false)->comment('Птелеграмм Удалить'),
            'message_create' => $this->boolean()->defaultValue(false)->comment('Сообщения Добавление'),
            'message_update' => $this->boolean()->defaultValue(false)->comment('Сообщения Изменение'),
            'message_view' => $this->boolean()->defaultValue(false)->comment('Сообщения Просмотр'),
            'message_view_all' => $this->boolean()->defaultValue(false)->comment('Сообщения Просмотр всех'),
            'message_delete' => $this->boolean()->defaultValue(false)->comment('Сообщения Удалить'),
            'recalls_create' => $this->boolean()->defaultValue(false)->comment('Отзовы Добавление'),
            'recalls_update' => $this->boolean()->defaultValue(false)->comment('Отзовы Изменение'),
            'recalls_view' => $this->boolean()->defaultValue(false)->comment('Отзовы Просмотр'),
            'recalls_view_all' => $this->boolean()->defaultValue(false)->comment('Отзовы Просмотр всех'),
            'recalls_delete' => $this->boolean()->defaultValue(false)->comment('Отзовы Удалить'),
            'mailing_create' => $this->boolean()->defaultValue(false)->comment('Рассылка Добавление'),
            'mailing_update' => $this->boolean()->defaultValue(false)->comment('Рассылка Изменение'),
            'mailing_view' => $this->boolean()->defaultValue(false)->comment('Рассылка Просмотр'),
            'mailing_view_all' => $this->boolean()->defaultValue(false)->comment('Рассылка Просмотр всех'),
            'mailing_delete' => $this->boolean()->defaultValue(false)->comment('Рассылка Удалить'),
            'books' => $this->boolean()->defaultValue(false)->comment('Справочники'),
        ]);

        $this->insert('role', [
            'name' => 'Администратор',
            'client_wh_create' => 1,
            'client_wh_update' => 1,
            'client_wh_view' => 1,
            'client_wh_view_all' => 1,
            'client_wh_delete' => 1,
            'client_tg_create' => 1,
            'client_tg_update' => 1,
            'client_tg_view' => 1,
            'client_tg_view_all' => 1,
            'client_tg_delete' => 1,
            'message_create' => 1,
            'message_update' => 1,
            'message_view' => 1,
            'message_view_all' => 1,
            'message_delete' => 1,
            'recalls_create' => 1,
            'recalls_update' => 1,
            'recalls_view' => 1,
            'recalls_view_all' => 1,
            'recalls_delete' => 1,
            'mailing_create' => 1,
            'mailing_update' => 1,
            'mailing_view' => 1,
            'mailing_view_all' => 1,
            'mailing_delete' => 1,
            'books' => 1,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('role');
    }
}
