<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210424_052519_create_recalls_table`.
 */
class m210424_052519_create_recalls_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('recalls', [
            'id' => $this->primaryKey(),
            'client_wh_id' => $this->integer()->comment('Пользователь'),
            'type' => $this->string()->comment('Тип'),
            'text' => $this->binary()->comment('Текст'),
            'create_at' => $this->datetime()->comment('Создан'),
            'who' => $this->integer()->comment('Кто написал'),
        ]);

        $this->createIndex(
            'idx-recalls-client_wh_id',
            'recalls',
            'client_wh_id'
        );
                        
        $this->addForeignKey(
            'fk-recalls-client_wh_id',
            'recalls',
            'client_wh_id',
            'client_wh',
            'id',
            'SET NULL'
        );
                        $this->createIndex(
            'idx-recalls-who',
            'recalls',
            'who'
        );
                        
        $this->addForeignKey(
            'fk-recalls-who',
            'recalls',
            'who',
            'client_tg',
            'id',
            'SET NULL'
        );
                        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-recalls-client_wh_id',
            'recalls'
        );
                        
        $this->dropIndex(
            'idx-recalls-client_wh_id',
            'recalls'
        );
                        
                        $this->dropForeignKey(
            'fk-recalls-who',
            'recalls'
        );
                        
        $this->dropIndex(
            'idx-recalls-who',
            'recalls'
        );
                        
                        
        $this->dropTable('recalls');
    }
}
