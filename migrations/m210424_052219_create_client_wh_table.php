<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210424_052219_create_client_wh_table`.
 */
class m210424_052219_create_client_wh_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('client_wh', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Имя'),
            'phone' => $this->string()->comment('Телефон'),
            'create_at' => $this->datetime()->comment('Создан'),
            'tag' => $this->string()->comment('Хэш тэк'),
        ]);

        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
        $this->dropTable('client_wh');
    }
}
