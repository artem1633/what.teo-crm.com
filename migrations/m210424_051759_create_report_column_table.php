<?php
use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m210424_051759_create_report_column_table extends Migration
{
    /**
     * @inheritdoc 
     */
    public function up()
    {
        $this->createTable('report_column', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'client_wh_name' => $this->boolean()->defaultValue(false)->comment('Пвотцап Имя'),
            'client_wh_phone' => $this->boolean()->defaultValue(false)->comment('Пвотцап Телефон'),
            'client_wh_create_at' => $this->boolean()->defaultValue(false)->comment('Пвотцап Создан'),
            'client_wh_tag' => $this->boolean()->defaultValue(false)->comment('Пвотцап Хэш тэк'),
            'client_tg_name' => $this->boolean()->defaultValue(false)->comment('Птелеграмм Имя'),
            'client_tg_phone' => $this->boolean()->defaultValue(false)->comment('Птелеграмм Телеграмм id'),
            'client_tg_create_at' => $this->boolean()->defaultValue(false)->comment('Птелеграмм Создан'),
            'client_tg_step' => $this->boolean()->defaultValue(false)->comment('Птелеграмм step'),
            'client_tg_other' => $this->boolean()->defaultValue(false)->comment('Птелеграмм other'),
            'client_tg_wh' => $this->boolean()->defaultValue(false)->comment('Птелеграмм wh'),
            'message_client_wh_id' => $this->boolean()->defaultValue(false)->comment('Сообщения Пользователь'),
            'message_text' => $this->boolean()->defaultValue(false)->comment('Сообщения Текст'),
            'message_create_at' => $this->boolean()->defaultValue(false)->comment('Сообщения Дата'),
            'recalls_client_wh_id' => $this->boolean()->defaultValue(false)->comment('Отзовы Пользователь'),
            'recalls_type' => $this->boolean()->defaultValue(false)->comment('Отзовы Тип'),
            'recalls_text' => $this->boolean()->defaultValue(false)->comment('Отзовы Текст'),
            'recalls_create_at' => $this->boolean()->defaultValue(false)->comment('Отзовы Создан'),
            'recalls_who' => $this->boolean()->defaultValue(false)->comment('Отзовы Кто написал'),
            'mailing_name' => $this->boolean()->defaultValue(false)->comment('Рассылка Наименование'),
            'mailing_text' => $this->boolean()->defaultValue(false)->comment('Рассылка Текст'),
            'mailing_status' => $this->boolean()->defaultValue(false)->comment('Рассылка Статус'),
        ]);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('report_column');
    }
}
