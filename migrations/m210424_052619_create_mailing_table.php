<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210424_052619_create_mailing_table`.
 */
class m210424_052619_create_mailing_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('mailing', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'text' => $this->binary()->comment('Текст'),
            'status' => $this->string()->comment('Статус'),
        ]);

        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
        $this->dropTable('mailing');
    }
}
