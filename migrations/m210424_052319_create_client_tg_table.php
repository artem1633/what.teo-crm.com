<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210424_052319_create_client_tg_table`.
 */
class m210424_052319_create_client_tg_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('client_tg', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Имя'),
            'phone' => $this->string()->comment('Телеграмм id'),
            'create_at' => $this->datetime()->comment('Создан'),
            'step' => $this->string()->comment('step'),
            'other' => $this->string()->comment('other'),
            'wh' => $this->string()->comment('wh'),
        ]);

        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
        $this->dropTable('client_tg');
    }
}
