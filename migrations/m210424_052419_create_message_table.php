<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210424_052419_create_message_table`.
 */
class m210424_052419_create_message_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('message', [
            'id' => $this->primaryKey(),
            'client_wh_id' => $this->integer()->comment('Пользователь'),
            'text' => $this->binary()->comment('Текст'),
            'create_at' => $this->datetime()->comment('Дата'),
        ]);

        $this->createIndex(
            'idx-message-client_wh_id',
            'message',
            'client_wh_id'
        );
                        
        $this->addForeignKey(
            'fk-message-client_wh_id',
            'message',
            'client_wh_id',
            'client_wh',
            'id',
            'SET NULL'
        );
                        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-message-client_wh_id',
            'message'
        );
                        
        $this->dropIndex(
            'idx-message-client_wh_id',
            'message'
        );
                        
                        
        $this->dropTable('message');
    }
}
