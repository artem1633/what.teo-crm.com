<?php

namespace app\modules\api\models;

use Yii;
use app\models\MobileUser;
use yii\base\Model;

/**
 * Class Login
 * @package app\modules\api\models
 */
class Login extends Model
{
    public $login;
    public $password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'password'], 'required'],
            [['login', 'password'], 'string'],
        ];
    }

    /**
     * @return string
     */
    public function login()
    {
        if($this->validate()){
            $token = md5($this->login).md5($this->password);
            $user = MobileUser::find()->where(['token' => $token])->one();

            if($user){
                return $token;
            } else {
                return null;
            }
        }

        return null;
    }
}