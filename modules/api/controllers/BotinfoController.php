<?php

namespace app\modules\api\controllers;

use app\models\Chat;
use app\models\DailySchedualed;
use app\models\JobTable;
use app\models\Langs;
use app\models\Message;
use app\models\Recalls;
use app\models\Pay;
use app\models\SetPayoutDeduction;
use app\models\Settings;
use app\models\ClientTg;
use app\models\ClientWh;
use app\models\QuizValue;
use app\models\QuizUser;
use app\models\Mailing;
use app\models\Quiz;
use app\models\VideoMotivashki;
use app\models\WithdrawalRequests;
use app\models\Words;
use yii\rest\ActiveController;
use yii\web\Response;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use Yii;

/**
 * Default controller for the `api` module
 */
class BotinfoController extends ActiveController
{
    public $modelClass = 'app\models\Api';

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => ['bot-in', 'testpush', 'bot-update', 'send-refer', 'check-send'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBotIn()
    {


        $content = file_get_contents('php://input'); //интересная строка: означает, что мы поточно воспринимаем запрос, который к нам пришел. Подробнее после)
        $report = json_decode($content); //интересная строка: означает, что мы поточно воспринимаем запрос, который к нам пришел. Подробнее после)
        $result = json_decode($content, true); //декодируем апдейт json, пришедший с телеграмма
        $text2 = '';
        $chat_id = false;
        if (isset($result["message"])) {
            if (isset($result["message"]["text"])) {
                $text = $result["message"]["text"]; //Текст сообщения
                $text2 = $result["message"]["text"];
            } else{
               return true; 
            }
            if (isset($result["message"]["chat"]["username"])) {
                $username = $result["message"]["chat"]["username"]; //Уникальный идентификатор пользователя
            }

            $chat_id = $result["message"]["chat"]["id"]; //Уникальный идентификатор пользователя
            $name = $result["message"]["from"]["first_name"]; //Юзернейм пользователя
            $typeM = $result["message"]["chat"]["type"];
        }


        /** @var UsersList $user */
        $message_id = null;
        if (isset($result["callback_query"])){   
            if ($result["callback_query"]) {
                $text = $result["callback_query"]['data'];
                $text2 = $result["callback_query"]["message"]["text"];
                $chat_id = $result['callback_query']['message']['chat']['id'];
                $message_id = $result['callback_query']['message']['message_id'];
            }
        }
        if (!$chat_id) {
            return true;
        }
        $user = ClientTg::find()->where(['phone' => $chat_id])->one();
        if (!$user) {
            $user = new ClientTg([
                'name' => $name,
                'phone' => $chat_id,
                'create_at' => date('Y-m-d H:i:s'),
            ]);
            if (!$user->save(false)) {
                $a = serialize($user->errors);
                return true;
            }
        }

//пробиваем из канала телефон
        $pos = strpos($text, '/start ');
        if ($pos !== false) {
            $pos2 = strpos($text, '/start ');
            $rest = substr($text, $pos2+7);
            $wh = ClientWh::find()->where(['phone' => '+'.$rest])->one();
            if (!$wh) {
                $this->getReq('sendMessage', [
                    'chat_id' => $chat_id,
                    'text' => 'Такой номер не найдет',
                    'parse_mode' => 'HTML',
                    'reply_markup' => json_encode([
                        'inline_keyboard' => [
                            [
                                ['text' => 'Пробить номер', 'callback_data' => 'check'],],
                                [['text' => 'Создать пост', 'callback_data' => 'post'],
                            ],
                        ],
                    ])
                ]);

                return true;
            }
            $user->step = 'list_recalls_'.$wh->id;
            $text = 'list_recalls_'.$wh->id;
        }
//Пробиваем пользователя как из бота так и из группы
        $pos = strpos($text, '/check ');
        if ($pos !== false) {

            $pos2 = strpos($text, '/check ');
            $rest = substr($text, $pos2+7);

            if (!$this->validPhone($rest)) {
                $this->getReq('sendMessage', [
                    'chat_id' => $chat_id,
                    'text' => "Номер телефона не валидный нужно вводить +79999999999 \nВведите верный номер телефона",
                    'parse_mode' => 'Markdown',
                ]);
                return true;
            }

            $wh = ClientWh::find()->where(['phone' => $rest])->one();
            if (!$wh) {
                $this->getReq('sendMessage', [
                    'chat_id' => $chat_id,
                    'text' => 'Такой номер не найдет',
                    'parse_mode' => 'HTML',
                ]);

                return true;
            }
            //пометка где сейчас пользователь
            $user->step = 'check';
            $text = $rest;
        }
        //пометка где сейчас пользователь
        if ($text == 'Пробить номер' && $typeM != 'group') {
            $text = 'check';
        }

        if ($text == 'Создать пост' && $typeM != 'group') {
            $text = 'post';
        }
        //первый раз вступил в бота
        if ($text == '/start') {
            //если пользователь существует очищаем историю, можно закоментировать 
            if ($user){
                $user->delete();
                $user = new ClientTg([
                    'name' => $name,
                    'phone' => $chat_id,
                    'create_at' => date('Y-m-d H:i:s'),
                ]);

                if (!$user->save(false)) {
                    $a = serialize($user->errors);
                    return true;
                }
            }


                $lan1[] = ["id"  => '1','text'=>'Пробить номер'];
                $lan2[] = ["id"  => '2','text'=>'Создать пост'];
                $this->getReq('sendMessage', [
                    'chat_id' => $chat_id,
                    'text' => 'Здравствуйте!
Вступайте в наш канал @channel
Или пробейте человека по его номеру телефона, для этого нажмите на кнопку
',
                    'parse_mode' => 'HTML',
                    'reply_markup' => json_encode([
                        'keyboard' => [$lan1,$lan2], 

                        "one_time_keyboard" => true,
                        "resize_keyboard" => true
                    ]),
                ]);

                return true;
        }  
        
        // если пользователь на этапе пробива номера и мы получили телефон
        if ($user->step == 'check') {

            if (!$this->validPhone($text)) {
                $this->getReq('sendMessage', [
                    'chat_id' => $chat_id,
                    'text' => "Номер телефона не валидный нужно вводить +79999999999 \nВведите верный номер телефона",
                    'parse_mode' => 'Markdown',
                ]);
                return true;
            }

            $user->step = '';
            $user->save(false);
            $wh = ClientWh::find()->where(['phone' => $text])->one();
            // если пользователь есть в базе
            if ($wh) {
                $countM = Message::find()->where(['client_wh_id' => $wh->id])->count();
                $countUP = Recalls::find()->where(['client_wh_id' => $wh->id,'type' => 'Положительный'])->count();
                $countDOWN = Recalls::find()->where(['client_wh_id' => $wh->id,'type' => 'Отрицательный'])->count();
                if ($typeM != 'group') {

                    $this->getReq('sendMessage', [
                        'chat_id' => $chat_id,
                        'text' => "*Имя*: {$wh->name}
    *Телефон*: {$wh->phone}
    *Дата внесения в базу*: ".date('d.m.Y',strtotime($wh->create_at))."
    *Количество сообщений в базе*: {$countM}
            
    Отзывы: +{$countUP}/-{$countDOWN}
        ",
                        'parse_mode' => 'Markdown',
                        'reply_markup' => json_encode([
                            'inline_keyboard' => [
                                [
                                    ['text' => 'Посмотреть отзывы', 'callback_data' => "list_recalls_".$wh->id],
                                ],
                            ],
                        ])
                    ]);

                    return true;
                } else {
                    $wh->phone = str_replace('+7', '7', $wh->phone);

                   $this->getReq('sendMessage', [
                        'chat_id' => $chat_id,
                        'text' => "*Имя*: {$wh->name}
    *Телефон*: {$wh->phone}
    *Дата внесения в базу*: ".date('d.m.Y',strtotime($wh->create_at))."
    *Количество сообщений в базе*: {$countM}
            
    Отзывы: +{$countUP}/-{$countDOWN}
        ",
                        'parse_mode' => 'Markdown',
                        'reply_markup' => json_encode([
                            'inline_keyboard' => [
                                [
                                    ['text' => 'Посмотреть отзывы', 'url' => "https://t.me/WhaTestBot?start={$wh->phone}"],
                                ],
                            ],
                        ])
                    ]);

                    return true; 
                }
            } else {

                if ($typeM != 'group') {
                    //если номера нет в базе
                    $lan1[] = ["id"  => '1','text'=>'Пробить номер'];
                    $lan2[] = ["id"  => '2','text'=>'Создать пост'];
                    $this->getReq('sendMessage', [
                        'chat_id' => $chat_id,
                        'text' => 'Такой номер не найдет',
                        'parse_mode' => 'HTML',

                        'reply_markup' => json_encode([
                            'keyboard' => [$lan1,$lan2],

                            "one_time_keyboard" => true,
                            "resize_keyboard" => true
                        ]),
                    ]);
                    return true;
                }
                //если номера нет в базе
                $lan1[] = ["id"  => '1','text'=>'Пробить номер'];
                $lan2[] = ["id"  => '2','text'=>'Создать пост'];
                $this->getReq('sendMessage', [
                    'chat_id' => $chat_id,
                    'text' => 'Такой номер не найдет',
                    'parse_mode' => 'HTML',
                ]);
                return true;
                
            }

        }
        // листаем отзовы
        $pos = strpos($text, 'list_recalls');
        $re = false;
        if ($pos !== false) {
            $pos2 = strpos($text, 's_');
            $pos11 = strpos($text, '&');
            if ($pos11 !== false) {
                $rest = substr($text, $pos2+2,$pos11-2-$pos2);
            } else {
                $rest = substr($text, $pos2+2);
            }

            $page = 0;

            $pos3 = strpos($text, 'page_');
            if ($pos3 !== false) { // Получаем текущую страницу пагинации
            	$pos3 = strpos($text, 'page_');
            	$page = substr($text, $pos3+5);
            }

            if (!$re){
                $re = Recalls::find()->where(['client_wh_id' => $rest])->one();
            }
            $textM = "";

            $dataProvider = new ArrayDataProvider([
            	'allModels' => Recalls::find()->where(['client_wh_id' => $rest])->all(),
            	'pagination' => [
            		'page' => $page,
            		'pageSize' => 10,
            	], 
            ]);

            // если в базе есть отзовы
            if($dataProvider->totalCount > 0){
                //формируем текст сообщений
                foreach ($dataProvider->models as $model) {
    	              if ($model->type == 'Положительный') {
    	                $textM .= "

    	".hex2bin('F09F918D')."*Отзыв от ".date('d.m.Y',strtotime($model->create_at))."*:
    	{$model->text}";
    	            }
    	            if ($model->type == 'Отрицательный') {
    	                $textM .= "

    	".hex2bin('F09F918E')."*Отзыв от ".date('d.m.Y',strtotime($model->create_at))."*:
    	{$model->text}";
    	            }
                }



                //если это не первая страница(редактирование)
                if ($message_id){
                    $buttoms = [];

                    // проверка что это не последняя страница и не первая
                    if($page != 0 && $page != $dataProvider->pagination->pageCount){
                        $pageButtons = [
                                'inline_keyboard' => [
                                    [
                                        ['text' => 'Назад', 'callback_data' => 'list_recalls_'.$rest.'&page_'.($page-1)],
                                        ['text' => 'Вперед', 'callback_data' =>'list_recalls_'.$rest.'&page_'.($page+1)]
                                    ],
                                    [
                                        ['text' => 'Оставить отзыв', 'callback_data' => 'send_recalls_'.$rest],
                                    ],
                                ],
                            ];
                    }
                    //если это первая страница
                    if($page == 0 && $page != $dataProvider->pagination->pageCount){
                        $pageButtons = [
                                'inline_keyboard' => [
                                    [
                                        ['text' => 'Вперед', 'callback_data' =>'list_recalls_'.$rest.'&page_'.($page+1)]
                                    ],
                                    [
                                        ['text' => 'Оставить отзыв', 'callback_data' => 'send_recalls_'.$rest],
                                    ],
                                ],
                            ];
                    }
                    // если это последняя страница
                    if($page != 0 && $page == $dataProvider->pagination->pageCount-1){
                        $pageButtons = [
                                'inline_keyboard' => [
                                    [
                                        ['text' => 'Назад', 'callback_data' => 'list_recalls_'.$rest.'&page_'.($page-1)],
                                    ],
                                    [
                                        ['text' => 'Оставить отзыв', 'callback_data' => 'send_recalls_'.$rest],
                                    ],
                                ],
                            ];
                    }

                    $this->getReq('editMessageText', [
                        'chat_id' => $chat_id,
                        'message_id' => $message_id,
                        'text' => $textM,
                        'parse_mode' => 'Markdown',
                        'reply_markup' => json_encode($pageButtons)
                    ]);
                } else {
                    // Если это первый пост а не редактирование
                    if ($dataProvider->pagination->pageCount > 1) {
                        $this->getReq('sendMessage', [
                            'chat_id' => $chat_id,
                            'text' => $textM,
                            'parse_mode' => 'Markdown',
                            'reply_markup' => json_encode([
                                'inline_keyboard' => [
                                    [
                                        ['text' => 'Вперед', 'callback_data' =>'list_recalls_'.$rest.'&page_1'],],
                                        [
                                        ['text' => 'Оставить отзыв', 'callback_data' => 'send_recalls_'.$rest],
                                    ],
                                ],
                            ])
                        ]);
                    } else {

                        $this->getReq('sendMessage', [
                            'chat_id' => $chat_id,
                            'text' => $textM,
                            'parse_mode' => 'Markdown',
                            'reply_markup' => json_encode([
                                'inline_keyboard' => [
                                        [
                                            ['text' => 'Оставить отзыв', 'callback_data' => 'send_recalls_'.$rest],
                                    ],
                                ],
                            ])
                        ]);
                    }
                }  
            } else {
                    $this->setClear($chat_id, $message_id);

                //если нету отзывов
                $this->getReq('sendMessage', [
                    'chat_id' => $chat_id,
                    'text' => 'Нету отзывов',
                    'parse_mode' => 'HTML',
                    'reply_markup' => json_encode([
                        'inline_keyboard' => [
                                [
                                ['text' => 'Оставить отзыв', 'callback_data' => 'send_recalls_'.$rest],
                            ],
                        ],
                    ])
                ]);
            }
            

            return true;
        }

        // если на этапе оставить озыв
        $pos = strpos($text, 'send_recalls');
        if ($pos !== false or $user->step == 'send_recalls') {
            // проверяем что заполнен тип
            if ($text == 'up') {
                // ставим пометку что выбрал пользователь
                $user->other = 'Положительный';
                $user->save(false); 
                $this->setClear($chat_id, $message_id);
                $this->getReq('sendMessage', [
                    'chat_id' => $chat_id,
                    'text' => 'Напишите текст отзыва',
                    'parse_mode' => 'HTML',
                ]);
                return true;
            }
            if ($text == 'down') {
                // ставим пометку что выбрал пользователь
                $user->other = 'Отрицательный';
                $user->save(false); 
                $this->setClear($chat_id, $message_id);
                $this->getReq('sendMessage', [
                    'chat_id' => $chat_id,
                    'text' => 'Напишите текст отзыва',
                    'parse_mode' => 'HTML',
                ]);
                return true;
            }
            // если написали текст отзыва
            if ($user->other != '') {
                $re = new Recalls([
                    'client_wh_id' => $user->wh,
                    'type' => $user->other,
                    'text' => $text,
                    'who' => $user->id,
                    'create_at' => date('Y-m-d H:i:s'),
                ]); 
                $re->save(false);


                $user->step = '';
                $user->wh = '';
                $user->other = '';
                $user->save(false);
                
                $lan1[] = ["id"  => '1','text'=>'Пробить номер'];
                $lan2[] = ["id"  => '2','text'=>'Создать пост'];
                $this->getReq('sendMessage', [
                    'chat_id' => $chat_id,
                    'text' => 'Спасибо за отзыв',
                    'parse_mode' => 'HTML',

                    'reply_markup' => json_encode([
                        'keyboard' => [$lan1,$lan2],

                        "one_time_keyboard" => true,
                        "resize_keyboard" => true
                    ]),
                ]);
                return true;

            }
            $pos2 = strpos($text, 's_');
            $rest = substr($text, $pos2+2);

            $user->step = 'send_recalls';
            $user->wh = $rest;
            $user->save(false);

            $this->setClear($chat_id, $message_id);
            // если начало написания отзыва
            $this->getReq('sendMessage', [
                'chat_id' => $chat_id,
                'text' => 'Выберите тип отзыва',
                'parse_mode' => 'HTML',
                'reply_markup' => json_encode([
                    'inline_keyboard' => [
                        [
                            ['text' => 'Положительный', 'callback_data' => 'up'],],
                            [['text' => 'Отрицательны', 'callback_data' => 'down'],
                        ],
                    ],
                ])
            ]);
            return true;
        }


        // пробитие номер 
        if ($text == 'check') {
            $user->step = 'check';
            $user->save(false);
            $this->setClear($chat_id, $message_id);
            $this->getReq('sendMessage', [
                'chat_id' => $chat_id,
                'text' => 'Введите номер телефона',
                'parse_mode' => 'HTML',
            ]);
            return true;
        }

        //публикация поста
        if ($user->step == 'post2') {
            $user->step = 'post3';
            $user->other = $text;
            $user->save(false);
            $this->setClear($chat_id, $message_id);
            $this->getReq('sendMessage', [
                'chat_id' => $chat_id,
                'text' => 'Введите номер телефона',
                'parse_mode' => 'HTML',
            ]);
            return true;
        }
        $phone2 = '';
        // публикация поста оставляем номер телефона
        if ($user->step == 'post3') {
            if (!$this->validPhone($text)) {
                $this->getReq('sendMessage', [
                    'chat_id' => $chat_id,
                    'text' => "Номер телефона не валидный нужно вводить +79999999999 \nВведите верный номер телефона",
                    'parse_mode' => 'Markdown',
                ]);
                return true;
            }
            $wh = ClientWh::find()->where(['phone' => $text])->one();
            if (!$wh) {
                $wh = new ClientWh([
                    'name' => $user->name,
                    'phone' => $text,
                    'create_at' => date('Y-m-d H:i:s'),
                ]);
                $wh->save(false);
            }
            //проверяем публиковался ли недавно такой пост
            $re = Message::find()->where(['client_wh_id' => $wh->id,'text' => $user->other])->andwhere(['>', 'create_at', date('Y-m-d H:i:s', strtotime('-1 days',strtotime(date('Y-m-d H:i:s'))))])->one();
            if ($re) {
                $user->other = '';
                $user->step = '';
                $user->save(false);

                $lan1[] = ["id"  => '1','text'=>'Пробить номер'];
                $lan2[] = ["id"  => '2','text'=>'Создать пост'];
                $this->getReq('sendMessage', [
                    'chat_id' => $chat_id,
                    'text' => 'Такой пост уже публиковался недавно',
                    'parse_mode' => 'HTML',

                    'reply_markup' => json_encode([
                        'keyboard' => [$lan1,$lan2],

                        "one_time_keyboard" => true,
                        "resize_keyboard" => true
                    ]),
                ]);

                return true;
            }
            // если пост не публиковался
            $re = new Message([
                'client_wh_id' => $wh->id,
                'text' => $user->other,
                'create_at' => date('Y-m-d H:i:s'),
            ]); 
            $re->save(false);

            $user->other = '';
            $user->step = '';
            $user->save(false);

            $lan1[] = ["id"  => '1','text'=>'Пробить номер'];
            $lan2[] = ["id"  => '2','text'=>'Создать пост'];
            $this->getReq('sendMessage', [
                'chat_id' => $chat_id,
                'text' => 'Пост опубликован',
                'parse_mode' => 'HTML',

                'reply_markup' => json_encode([
                    'keyboard' => [$lan1,$lan2],

                    "one_time_keyboard" => true,
                    "resize_keyboard" => true
                ]),
            ]);

            $countM = Message::find()->where(['client_wh_id' => $wh->id])->count();
            $countUP = Recalls::find()->where(['client_wh_id' => $wh->id,'type' => 'Положительный'])->count();
            $countDOWN = Recalls::find()->where(['client_wh_id' => $wh->id,'type' => 'Отрицательный'])->count();
            $tag = '';
            if ($wh->tag != null) {
                $tag = '
                '.$wh->tag;
            }
            // публикуем пост
            $this->getReq('sendMessage', [
                'chat_id' => '@gdg_wha_test',
                'text' => $re->text. "

*от {$user->name}*

*Отзывы* +{$countUP}/-{$countDOWN}
{$tag}
",
            'parse_mode' => 'Markdown',
            'reply_markup' => json_encode([
                'inline_keyboard' => [
                    [
                        ['text' => 'Посмотреть отзывы', 'url' => "https://t.me/WhaTestBot?start={$phone2}"],
                    ],
                ],
            ])
        ]);
            $this->getReq('sendMessage', [
                'chat_id' => '-548357477',
                'text' => $re->text. "

*от {$user->name}*

*Отзывы* +{$countUP}/-{$countDOWN}
{$tag}
",
            'parse_mode' => 'Markdown',
            'reply_markup' => json_encode([
                'inline_keyboard' => [
                    [
                        ['text' => 'Посмотреть отзывы', 'url' => "https://t.me/WhaTestBot?start={$phone2}"],
                    ],
                ],
            ])
        ]);


            return true;
        }
        //пост первый этап
        if ($text == 'post') {
            $user->step = 'post2';
            $user->save(false);
            $this->setClear($chat_id, $message_id);
            $this->getReq('sendMessage', [
                'chat_id' => $chat_id,
                'text' => 'Введите текст поста',
                'parse_mode' => 'HTML',
            ]);
            return true;
        }
            

        return true;
    }


    /**
     * Принимает информацию о новом реферальном посещении
     * @var integer $post
     * @var integer $lang
     * @return boolean
     */
    public function validPhone($text)
    {
 
        // $text = str_replace('8', '+7', $text);
        // $phone2 = str_replace('+7', '7', $text);

        // $pos2 = strpos($text, '/check ');
        // $rest = substr($text, $pos2+7);
        // $rest = str_replace('7', '+7', $rest);

        $pos = strpos($text, '+7');
        if ($pos === false) {
            return false;
        }
        if (strlen($text) != 12) {
            return false;
        }
        return true;
    }



    /**
     * Принимает информацию о новом реферальном посещении
     * @var integer $post
     * @var integer $lang
     * @return boolean
     */
    public function getMenu($user, $chat_id, $info = "Menu")
    {

        $this->getReq('sendMessage', [
            'chat_id' => $chat_id,
            'text' => $info,
            'parse_mode' => 'HTML',
            'reply_markup' => json_encode([
                'inline_keyboard' => [
                    [
                        ['text' => self::getWord(19, $user), 'callback_data' => 'my'],],
            [['text' =>self::getWord(20, $user), 'callback_data' => 'list'],
                    ],
                    [
                        ['text' => self::getWord(21, $user), 'callback_data' => 'info'],],
                        [['text' =>  self::getWord(22, $user), 'callback_data' => 'help'],
                    ],
                ],
            ])
        ]);
    }

    /**
     * Принимает информацию о новом реферальном посещении
     * @var integer $post
     * @var integer $lang
     * @return boolean
     */
    public function actionQ()
    {
        $content = file_get_contents('php://input'); //интересная строка: означает, что мы поточно воспринимаем запрос, который к нам пришел. Подробнее после)
        $report = json_decode($content); //интересная строка: означает, что мы поточно воспринимаем запрос, который к нам пришел. Подробнее после)
        $result = json_decode($content, true); 
        // $this->getReq('sendMessage', [
        //                 'chat_id' => '247187885',
        //                 'text' => serialize($result),
        //                 'parse_mode' => 'HTML',
        //             ]);
        // $this->getReq('sendMessage', [
        //                 'chat_id' => '247187885',
        //                 'text' => serialize($result["event"]),
        //                 'parse_mode' => 'HTML',
        //             ]);
        $message = '';
        if ($result["event"] == 'onAnyMessage' && isset($result["data"]["content"])) {
            $isGroupMsg = $result["data"]["isGroupMsg"];
            if (!$isGroupMsg){
                return true;
            }
              $name = $result["data"]["sender"]["pushname"];
              $phone = str_replace(' ', '', $result["data"]["sender"]["formattedName"]);
              $phone = str_replace('-', '', $phone);
              $phone2 = str_replace('+', '', $phone);
              $message = $result["data"]["content"];
              \Yii::warning($message); \Yii::warning($phone);\Yii::warning($name);
              // exit;
              $wh = ClientWh::find()->where(['phone' => $phone])->one();
              if (!$wh) {
                $wh = new ClientWh([
                    'name' => $name,
                    'phone' => $phone,
                    'create_at' => date('Y-m-d H:i:s'),
                ]);
                $wh->save(false);   
              }
              $re = Message::find()->where(['text' => $message, 'client_wh_id' => $wh->id])->andwhere(['>', 'create_at', date('Y-m-d H:i:s', strtotime('-1 days',strtotime(date('Y-m-d H:i:s'))))])->one();
              if (!$re) {
                $re = new Message([
                    'client_wh_id' => $wh->id,
                    'text' => $message,
                    'create_at' => date('Y-m-d H:i:s'),
                ]); 
                $re->save(false);
                $countM = Message::find()->where(['client_wh_id' => $wh->id])->count();
                $countUP = Recalls::find()->where(['client_wh_id' => $wh->id,'type' => 'Положительный'])->count();
                $countDOWN = Recalls::find()->where(['client_wh_id' => $wh->id,'type' => 'Отрицательный'])->count();
                $tag = '';
                if ($wh->tag != null) {
                    $tag = '
                    '.$wh->tag;
                }
                 $this->getReq('sendMessage', [
                    'chat_id' => '@gdg_wha_test',
                    'text' => $message. "

*от {$name}*
https://wa.me/{$phone2}

*Отзывы* +{$countUP}/-{$countDOWN}
{$tag}
",
                    'parse_mode' => 'Markdown',
                'disable_web_page_preview'=> true,
                    'reply_markup' => json_encode([
                        'inline_keyboard' => [
                            [
                                ['text' => 'Посмотреть отзывы', 'url' => "https://t.me/WhaTestBot?start={$phone2}"],
                            ],
                        ],
                    ])
                ]);

                 $this->getReq('sendMessage', [
                    'chat_id' => '-548357477',
                    'text' => $message. "

*от {$name}*
https://wa.me/{$phone2}

*Отзывы* +{$countUP}/-{$countDOWN}
{$tag}
",
                    'parse_mode' => 'Markdown',
                'disable_web_page_preview'=> true,
                    'reply_markup' => json_encode([
                        'inline_keyboard' => [
                            [
                                ['text' => 'Посмотреть отзывы', 'url' => "https://t.me/WhaTestBot?start={$phone2}"],
                            ],
                        ],
                    ])
                ]);

              }
        }
        return true;
    }


    /**
     * Принимает информацию о новом реферальном посещении 
     * @var integer $chat_id
     * @var integer $message_id
     * @return boolean
     */
    public function setClear($chat_id, $message_id)
    {
        if ($message_id) {
            $this->getReq('editMessageReplyMarkup', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => 'my text',
                'reply_markup' => null
            ]);
        }

        return true;
    }


    public function getReq($method, $params = [], $decoded = 0)
    { //параметр 1 это метод, 2 - это массив параметров к методу, 3 - декодированный ли будет результат будет или нет.

        $token = '1732596030:AAERqFug5IddAo7IAGvJcqygJ4pVcUfXa0A';//$token = '709328751:AAGWl7PwR5qg2uUlKmz7petTIfRtPBI9oBc';
        $url = "https://api.telegram.org/bot{$token}/{$method}"; //основная строка и метод
        if (count($params)) {
            $url = $url . '?' . http_build_query($params);//к нему мы прибавляем парметры, в виде GET-параметров
        }

        $curl = curl_init($url);    //инициализируем curl по нашему урлу
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,
            1);   //здесь мы говорим, чтобы запром вернул нам ответ сервера телеграмма в виде строки, нежели напрямую.
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);   //Не проверяем сертификат сервера телеграмма.
        curl_setopt($curl, CURLOPT_HEADER, 1);
        $result = curl_exec($curl);   // исполняем сессию curl
        curl_close($curl); // завершаем сессию
        return $result; //Или просто возращаем ответ в виде строки
    }

    public static function fullReq($method, $params = [], $decoded = 0)
    { //параметр 1 это метод, 2 - это массив параметров к методу, 3 - декодированный ли будет результат будет или нет.

        $token = Settings::find()->where(['key' => 'telegram_key'])->one()->value;//$token = '709328751:AAGWl7PwR5qg2uUlKmz7petTIfRtPBI9oBc';
        $url = "https://api.telegram.org/bot{$token}/{$method}"; //основная строка и метод
        if (count($params)) {
            $url = $url . '?' . http_build_query($params);//к нему мы прибавляем парметры, в виде GET-параметров
        }

        $curl = curl_init($url);    //инициализируем curl по нашему урлу
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,
            1);   //здесь мы говорим, чтобы запром вернул нам ответ сервера телеграмма в виде строки, нежели напрямую.
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);   //Не проверяем сертификат сервера телеграмма.
        curl_setopt($curl, CURLOPT_HEADER, 1);
        $result = curl_exec($curl);   // исполняем сессию curl
        curl_close($curl); // завершаем сессию
        return $result; //Или просто возращаем ответ в виде строки
    }

    public function actionWebhook()
    {
        $token = '1732596030:AAERqFug5IddAo7IAGvJcqygJ4pVcUfXa0A';//$token = '709328751:AAGWl7PwR5qg2uUlKmz7petTIfRtPBI9oBc';

        $url = 'https://api.telegram.org/bot' . $token . '/setWebhook?url=https://what.teo-crm.com/api/botinfo/bot-in';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
//        curl_setopt($ch, CURLOPT_PROXY, $proxy);
        //curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        $curl_scraped_page = curl_exec($ch);
        curl_close($ch);

        $content = file_get_contents('php://input'); //интересная строка: означает, что мы поточно воспринимаем запрос, который к нам пришел. Подробнее после)
        $result = json_decode($content, true); //декодируем апдейт json, пришедший с телеграмма
        var_dump($result);
        echo $curl_scraped_page;
    }

    public function actionCheck()
    {
        $mailin = Mailing::find()->where(['status' => 'Новая'])->one();
        $users = ClientTg::find()->all();
        if (!$mailin) {
            return 'нет рассылки';
        }
        foreach ($users as $value) {
            if ($value->phone && $value->phone > 0) {
                $this->getReq('sendMessage', [
                    'chat_id' => $value->phone,
                    'text' => $mailin->text,
                ]);
            }
        }
        $mailin->status = 'Выподнена';
        $mailin->save(false);
        return true;
    }


}
